(in-package national-digital-forecast-database)

(defconstant +greenwich-mean-time+ 0)
(defparameter *uri-scheme* "https")
(defparameter *uri-host* "graphical.weather.gov")
(defparameter *uri-path* "/xml/sample_products/browser_interface/ndfdXMLclient.php")

(deftype latitude () '(float -90.0 90.0))
(deftype longitude () '(float -180.0 180.0))
(deftype resolution () '(float 0 *))
(deftype product () '(member time-series glance))
(deftype universal-time () '(integer 0 *))
(deftype unit () '(member english metric))

(defstruct coordinate
  "A location defined as a point."
  (latitude 38.99225 :type latitude)
  (longitude -77.030639 :type longitude))
(defstruct subgrid
  "A location defined as a bounding-box defined by two coordinates."
  (lower-left (make-coordinate) :type coordinate)
  (upper-right (make-coordinate) :type coordinate)
  (resolution 5.0 :type single-float))
;; NDFD supports other ways of specifying locations
(deftype location () '(or (satisfies coordinate-p) (satisfies subgrid-p) (satisfies listp)))

(defstruct unsummarized-query
  "Partial representation of the ndfdXMLclient.php interface."
  (location (make-coordinate) :type location)
  (product 'glance :type product)
  (begin-time (get-universal-time) :type universal-time)
  (end-time (get-universal-time) :type universal-time)
  (unit 'english :type unit)
  (elements () :type list))

(defun location-to-query-string (location)
  "Converts a Location to the query substring need for an Unsummarized-query or Summarized-query."
  (cond ((coordinate-p location) (list (cons "lat" (coordinate-latitude location))
				       (cons "lon" (coordinate-longitude location))))
	((subgrid-p location) (with-slots (lower-left upper-right) location
				(list (cons "lat1" (coordinate-latitude lower-left))
				      (cons "lon1" (coordinate-longitude lower-left))
				      (cons "lat2" (coordinate-latitude upper-right))
				      (cons "lat2" (coordinate-longitude upper-right)))))
	((listp location) (error 'not-implemented))))
				
(defun universal-time-to-datetime-string (universal-time)
  "Converts Universal-time to the timedate format needed for an NDFD query."
  (multiple-value-bind
	(second minute hour date month year)
      (decode-universal-time universal-time +greenwich-mean-time+)
    (format nil "~04,'0d-~02,'0d-~02,'0dT~02,'0d:~02,'0d:~02,'0d" year month date hour minute second)))

(defun product-to-string (product)
  "Converts a Product to the format needed for an NDFD query."
  (ecase product
    (time-series "time-series")
    (glance "glance")))

(defun unit-to-string (unit)
  "Converts a Unit to the format needed for an NDFD query."
  (ecase unit
    (english "e")
    (metric "m")))
      
(defun elements-to-uri-query-list (elements)
  "Converts the Elements to a list needed to prepare the HTTP query string."
  (let ((query-list ()))
    (loop for element in elements
       do (let ((name (symbol-name element)))
	    (push (cons name name) query-list)))
    query-list))

(defun to-uri-query (ndfd-query)
  "Converts an Ndfd-Query to a HTTP query string"
  (with-slots (location
	       product
	       begin-time
	       end-time
	       unit
	       elements)
      ndfd-query
    (let ((query (list (cons "product" (product-to-string product))
		       (cons "begin" (universal-time-to-datetime-string begin-time))
		       (cons "end" (universal-time-to-datetime-string end-time))
		       (cons "Unit" (unit-to-string unit)))))
      (nconc query
	     (location-to-query-string location)
	     (elements-to-uri-query-list elements)))))

(defun to-uri (ndfd-query)
  "Converts an Ndfd-query to a URI"
  (quri:make-uri :scheme *uri-scheme*
		 :host *uri-host*
		 :path *uri-path*
		 :query (to-uri-query ndfd-query)))
