(defpackage national-digital-forecast-database
  (:nicknames ndfd)
  (:documentation "Partial representation of queries to the National Digital Forecast Database (NDFD).")
  (:use #:cl)
  (:export #:latitude
	   #:longitude
	   #:resolution
	   #:product #:time-series #:glance
	   #:unit #:english #:metric

	   #:coordinate
	   #:coordinate-latitude
	   #:coridinate-longitude
	   #:make-coordinate
	   #:coordinate-p

	   #:subgrid
	   #:subgrid-lower-left
	   #:subgrid-upper-right
	   #:subgrid-resolution
	   #:make-subgrid
	   #:subgrid-p

	   #:location

	   #:unsummarized-query
	   #:unsummarized-query-location
	   #:unsummarized-query-product
	   #:unsummarized-query-begin-time
	   #:unsummarized-query-end-time
	   #:unsummarized-query-unit
	   #:unsummarized-query-elements
	   #:make-unsummarized-query
	   #:unsummarized-query-p

	   #:to-uri))

(defpackage digital-weather-markup-language
  (:nicknames dwml)
  (:documentation "Partial representation of the Digital Weather Markup Language.")
  (:use #:cl)
  (:import-from #:local-time
		#:timestamp)
  (:export #:data
	   #:make-data
	   #:data-parameters
	   #:data-locations
	   #:data-time-layouts

	   #:summarization-type
	   #:string-to-summarization-type

	   #:string-to-time-coordinate-type

	   #:location
	   #:make-location
	   #:location-description
	   #:location-definition
	   #:location-key

	   #:point
	   #:make-point

	   #:time-layout
	   #:make-time-layout
	   #:time-layout-key
	   #:time-layout-start-times
	   #:time-layout-end-times

	   #:start-valid-time
	   #:make-start-valid-time
	   #:start-valid-time-name
	   #:start-valid-time-time

	   #:parameters
	   #:make-parameters
	   #:parameters-categories
	   #:parameters-temperatures

	   #:temperature
	   #:make-temperature
	   #:temperature-name
	   #:temperature-time-layout
	   #:temperature-values

	   #:temperature-type
	   #:string-to-temperature-type
	   #:maximum #:ekdmos-maximum
	   #:minimum #:ekdmos-minimum
	   #:hourly #:rtma-hourly
	   #:ekdmos-hourly
	   #:dew-point #:rtma-dew-point #:ekdmos-dew-point
	   #:head-index #:wind-chill #:apparent #:ekdmo-apparent
	   #:8-14-day-anomolies #:monthly-anomolies #:seasonal-anomolies

	   #:temperature-unit
	   #:string-to-temperature-unit
	   #:fahrenheit
	   #:celsius

	   #:temperature-value
	   #:make-temperature-value
	   #:temperature-value-value
	   #:temperature-upper-range
	   #:temperature-lower-range
	   #:temperature-data-source))

(defpackage digital-weather-markup-language-parser
  (:nicknames dwml-parser)
  (:documentation "Partial parser of the Digital Weather Markup Language.")
  (:use #:cl #:digital-weather-markup-language)
  (:import-from #:local-time
		#:timestamp
		#:parse-timestring)
  (:import-from #:parse-float
		#:parse-float)
  (:export #:initialize-parser
	   #:parse-dwml))
