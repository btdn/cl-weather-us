(in-package digital-weather-markup-language-parser)

(defparameter *slots-used-by-time-layout* '(start-valid-times end-valid-times))

(defmethod accept ((seed seed) (time-layout time-layout))
  (push time-layout
	(slot-value seed 'time-layouts)))

(defmethod accept-text ((seed seed) (time-layout time-layout) (text string))
  (assert-parse-stack-first seed :|layout-key|)
  (setf (time-layout-key time-layout) text))

(defmethod update ((seed seed) (time-layout time-layout))
  (setf (time-layout-start-times time-layout)
	(to-array-rev (slot-value seed 'start-valid-times)))
  (setf (time-layout-end-times time-layout)
	(to-array-rev (slot-value seed 'end-valid-times)))
  (clear-slots seed *slots-used-by-time-layout*))

(defun new-time-layout (name attributes seed)
  (declare (ignore name))
  (assert-parse-stack-first seed :|data|)
  (assert-slots-null seed *slots-used-by-time-layout*)
  (accept seed (make-time-layout :time-coordinate
			  (string-to-time-coordinate-type (assoc-get :|time-coordinate| attributes))
			  :summarization
			  (string-to-summarization-type (assoc-get :|summarization| attributes)))))

(defun finish-time-layout (name attributes seed)
  (declare (ignore name attributes))
  (assert-parse-stack-first seed :|time-layout|)
  (update seed (first (slot-value seed 'time-layouts))))

(defun text-layout-key (string seed)
  (assert-parse-stack-first seed :|layout-key|)
  (accept-text seed (first (slot-value seed 'time-layouts)) string))

(defmethod accept ((seed seed) (start-valid-time start-valid-time))
  (push start-valid-time
	(slot-value seed 'start-valid-times)))

(defmethod accept-text ((seed seed) (start-valid-time start-valid-time) (text string))
  (setf (start-valid-time-time start-valid-time)
	(parse-timestring text)))

(defun new-start-valid-time (name attributes seed)
  (declare (ignore name))
  (assert-parse-stack-first seed :|time-layout|)
  (accept seed (make-start-valid-time :name (assoc-get :|period-name| attributes))))

(defun text-start-valid-time (string seed)
  (assert-parse-stack-first seed :|start-valid-time|)
  (accept-text seed (first (slot-value seed 'start-valid-times)) string))

(defmethod accept-text ((seed seed) (null null) (text string))
  (assert-parse-stack-first seed :|end-valid-time|)
  (push (parse-timestring text)
	(slot-value seed 'end-valid-times)))

(defun text-end-valid-time (string seed)
  (assert-parse-stack-first seed :|end-valid-time|)
  (accept-text seed nil string))
