(in-package digital-weather-markup-language-parser)

(defparameter *slots-used-by-temperature* '(temperature-values))

(defmethod accept ((seed seed) (temperature temperature))
  (push temperature
	(slot-value seed 'temperatures)))

(defmethod accept-text ((seed seed) (temperature temperature) (text string))
  (setf (temperature-name (first-slot-value seed 'temperatures))
	text))

(defmethod update ((seed seed) (temperature temperature))
  (setf (temperature-values temperature)
	(to-array-rev (slot-value seed 'temperature-values)))
  (clear-slots seed *slots-used-by-temperature*))

(defun new-temperature (name attributes seed)
  (declare (ignore name))
  (assert-parse-stack-first seed :|parameters|)
  (assert-slots-null seed *slots-used-by-temperature*)
  (accept seed (make-temperature :type (assoc-get :|type| attributes #'string-to-temperature-type)
				 :unit (assoc-get :|units| attributes #'string-to-temperature-unit)
				 :time-layout (assoc-get :|time-layout| attributes))))

(defun finish-temperature (name attributes seed)
  (declare (ignore name attributes))
  (assert-parse-stack-first seed :|temperature|)
  (update seed (first-slot-value seed 'temperatures)))

(defmethod accept ((seed seed) (temperature-value temperature-value))
  (push temperature-value
	(slot-value seed 'temperature-values)))

(defmethod accept-text ((seed seed) (temperature-value temperature-value) (text string))
  (setf (temperature-value-value (first-slot-value seed 'temperature-values))
	(parse-integer text)))

(defun new-value_temperature (name attributes seed)
  (declare (ignore name))
  (assert-parse-stack-first seed :|temperature|)
  (accept seed (make-temperature-value :upper-range (assoc-get-optional :|upper-range| attributes #'parse-integer)
				       :lower-range (assoc-get-optional :|lower-range| attributes #'parse-integer))))
