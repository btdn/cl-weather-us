(in-package digital-weather-markup-language-parser)

(defparameter *slots-used-by-parameters* `(temperatures))

(defmethod update ((seed seed) (parameters parameters))
  (setf (parameters-temperatures parameters)
	(to-array-rev (slot-value seed 'temperatures)))
  (clear-slots seed *slots-used-by-parameters*))

(defun finish-parameters (name attributes seed)
  (declare (ignore name attributes))
  (update seed (data-parameters (slot-value seed 'data))))
