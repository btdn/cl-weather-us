(in-package digital-weather-markup-language-parser)

(defun text-name (string seed)
  (case (second (slot-value seed 'parse-stack))
    (:|temperature| (accept-text seed (first-slot-value seed 'temperatures) string))
    (otherwise seed)))

(defun new-value (name attributes seed)
  (case (first-slot-value seed 'parse-stack)
    (:|temperature| (new-value_temperature name attributes seed))
    (otherwise seed)))

(defun text-value (string seed)
  (case (second (slot-value seed 'parse-stack))
    (:|temperature| (accept-text seed (first-slot-value seed 'temperature-values) string))
    (otherwise seed)))
