(in-package digital-weather-markup-language-parser)

(defparameter *new-element-hooks* (make-hash-table))
(defparameter *text-hooks* (make-hash-table))
(defparameter *finish-element-hooks* (make-hash-table))

(defun initialize-parser ()
  (dolist (table (list *new-element-hooks* *finish-element-hooks* *text-hooks*))
    (clrhash table))
  (dolist (hooks '((:|data| new-data finish-data)
		   (:|description| nil nil text-description)
		   (:|end-valid-time| nil nil text-end-valid-time)
		   (:|location| new-location)
		   (:|location-key| nil nil text-location-key)
		   (:|layout-key| nil nil text-layout-key)
		   (:|name| nil nil text-name)
		   (:|parameters| nil finish-parameters)
		   (:|point| nil finish-point)
		   (:|time-layout| new-time-layout finish-time-layout)
		   (:|start-valid-time| new-start-valid-time nil text-start-valid-time)
		   (:|temperature| new-temperature finish-temperature)
		   (:|value| new-value nil text-value)))
    (multiple-value-bind (keyword new-element-hook finish-element-hook text-hook)
	(values-list hooks)
      (when new-element-hook
	(setf (gethash keyword *new-element-hooks*) new-element-hook))
      (when finish-element-hook
	(setf (gethash keyword *finish-element-hooks*) finish-element-hook))
      (when text-hook
	(setf (gethash keyword *text-hooks*) text-hook)))))

(defun new-element-hook (name attributes seed)
  (update-seed (funcall (gethash name *new-element-hooks* #'third-arg)
			name attributes seed)
	       :push name))

(defun finish-element-hook (name attributes parent-seed seed)
  (declare (ignore parent-seed))
  (update-seed (funcall (gethash name *finish-element-hooks* #'third-arg)
			name attributes seed)
	       :pop name))

(defun text-hook (string seed)
  (funcall (gethash (first (slot-value seed 'parse-stack)) *text-hooks* #'second-arg)
	   string seed)
  seed)

(defun parse-dwml (in)
  (slot-value (s-xml:start-parse-xml in
			 (make-instance 's-xml:xml-parser-state
					:seed (make-instance 'seed)
					:new-element-hook #'new-element-hook
					:finish-element-hook #'finish-element-hook
					:text-hook #'text-hook))
	      'data))
