(in-package digital-weather-markup-language-parser)

(defclass seed ()
  ((parse-stack :initform ()
		:type list)

   (locations :initform ()
	      :type list)

   (time-layouts :initform ()
		:type list)
   (start-valid-times :initform ()
		      :type list)
   (end-valid-times :initform ()
		    :type list)

   (temperatures :initform ()
		 :type list)
   (temperature-values :initform ()
		       :type list)

   (data :initform nil
	 :type (or null data))))

(defun assert-parse-stack-first (seed expected)
  (assert (eq expected (first (slot-value seed 'parse-stack)))))

(defun assert-slots-null (seed slot-list)
  (dolist (slot slot-list)
    (check-type (slot-value seed slot) null)))

(defun to-array-rev (list)
  (check-type list sequence)
  (make-array (length list) :element-type (type-of (first list)) :initial-contents (reverse list)))

(defun clear-slots (object slot-list)
  (dolist (slot slot-list)
    (setf (slot-value object slot) nil))) 

(defgeneric accept (seed object))
(defgeneric accept-text (seed object text))
(defgeneric update (seed object))

(defmethod accept :around (seed object)
  (call-next-method seed object)
  seed)
(defmethod accept-text :around (seed object text)
  (call-next-method seed object text)
  seed)
(defmethod update :around (seed object)
  (call-next-method seed object)
  seed)

(defmethod accept ((seed seed) (text string))
  (check-type (slot-value seed 'text) null)
  (setf (slot-value seed 'text) text))

(defun assoc-get (key assoc &optional parser-fun)
  (let ((value (cdr (assoc key assoc))))
    (if parser-fun
	(funcall parser-fun value)
	value)))

(defun assoc-get-optional (key assoc &optional parser-fun)
  (let ((value (cdr (assoc key assoc))))
    (when value
      (if parser-fun
	  (funcall parser-fun value)
	  value))))

(defun second-arg (first second)
  (declare (ignore first))
  second)
(defun third-arg (first second third)
  (declare (ignore first second))
  third)

(defun update-seed (seed push-or-pop value)
  (ecase push-or-pop
    (:push (push value (slot-value seed 'parse-stack)))
    (:pop (pop (slot-value seed 'parse-stack))))
  seed)

(defun first-slot-value (object slot-name)
  (first (slot-value object slot-name)))
