(in-package digital-weather-markup-language-parser)

(defmethod accept ((seed seed) (location location))
  (push location
	(slot-value seed 'locations)))

(defmethod accept-text ((seed seed) (location location) (text string))
  (ecase (first-slot-value seed 'parse-stack)
    (:|location-key| (setf (location-key location) text))
    (:|description| (setf (location-description location) text))))

(defun new-location (name attributes seed)
  (declare (ignore name attributes))
  (assert-parse-stack-first seed :|data|)
  (accept seed (make-location)))

(defun text-location-key (string seed)
  (assert-parse-stack-first seed :|location-key|)
  (accept-text seed (first-slot-value seed 'locations) string))

(defmethod accept ((seed seed) (point point))
  (setf (location-definition (first-slot-value seed 'locations)) point))
  
(defun finish-point (name attributes seed)
  (declare (ignore name))
  (assert-parse-stack-first seed :|point|)
  (accept seed
	  (make-point :latitude (parse-float (assoc-get :|latitude| attributes))
		      :longitude (parse-float (assoc-get :|longitude| attributes))
		      :summarization (string-to-summarization-type (assoc-get :|summarization| attributes)))))

(defun text-description (string seed)
  (assert-parse-stack-first seed :|description|)
  (accept-text seed (first-slot-value seed 'locations) string))
