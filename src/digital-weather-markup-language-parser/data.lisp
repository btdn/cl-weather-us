(in-package digital-weather-markup-language-parser)

(defparameter *slots-used-by-data* '(locations time-layouts))

(defmethod accept ((seed seed) (data data))
  (setf (slot-value seed 'data)
	data))

(defun new-data (name attributes seed)
  (declare (ignore name attributes))
  (assert-parse-stack-first seed :|dwml|)
  (accept seed (make-data)))

(defmethod update ((seed seed) (data data))
  (setf (data-locations data)
	(to-array-rev (slot-value seed 'locations)))
  (setf (data-time-layouts data)
	(to-array-rev (slot-value seed 'time-layouts)))
  (clear-slots seed *slots-used-by-data*))

(defun finish-data (name attributes seed)
  (declare (ignore name attributes))
  (assert-parse-stack-first seed :|data|)
  (update seed (slot-value seed 'data)))
	  
