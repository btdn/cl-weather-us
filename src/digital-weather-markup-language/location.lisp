(in-package digital-weather-markup-language)

(deftype radius-units () '(member statute-miles kilometers))
(deftype area-type () '(member circle rectangle))

(defstruct (point (:include national-digital-forecast-database:coordinate
			   (latitude)
			   (longitude)))
  (summarization nil :type (or null summarization-type)))

(defstruct nws-zone
  (state nil :type string)
  (summarization nil :type (or null summarization-type)))

(defstruct radius ;; This can't be right.
  (unit nil :type radius-units))

(defstruct circle
  (point nil :type point)
  (radius nil :type radius)
  (summarization nil :type (or null summarization-type)))

(defstruct rectangle
  (points nil :type (array point 4))
  (summarization nil :type (or null summarization-type)))

(defstruct area
  (definition nil :type (or circle rectangle))
  (type nil :type (or null area-type)))

(defstruct location
  (key nil :type (or null string))
  (description nil :type (or null string))
  (definition nil :type (or null point nws-zone area))
  (city nil :type (or null string))
  (area-description nil :type (or null string))) ;; also height, level or layer
