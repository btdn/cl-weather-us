(in-package digital-weather-markup-language)

(deftype time-coordinate-type () `(member utc
					  local))

(defun string-to-time-coordinate-type (string)
  (cond ((string= string "utc") 'utc)
	((string= string "local") 'local)))

(defstruct start-valid-time
  (name nil :type (or null string))
  (time nil :type (or null timestamp)))

(defstruct time-layout
  (time-coordinate nil :type (or null time-coordinate-type))
  (summarization nil :type (or null summarization-type))
  (key nil :type (or null string))
  (start-times (make-array 0 :element-type 'start-valid-time) :type (array start-valid-time))
  (end-times nil :type (or null (array timestamp))))
