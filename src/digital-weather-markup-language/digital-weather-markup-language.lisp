(in-package digital-weather-markup-language)

(defstruct data
  (parameters (make-parameters) :type parameters)
  (more-weather-information nil :type null) ;; not supported
  (locations (make-array 0 :element-type 'location) :type (array location))
  (time-layouts (make-array 0 :element-type 'time-layout) :type (array time-layout))
  (categories nil :type null)) ;; not supported

