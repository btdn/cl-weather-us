(in-package digital-weather-markup-language)

(deftype temperature-type () `(member maximum ekdmos-maximum
				      minimum ekdmos-minimum
				      hourly rtma-hourly
				      ekdmos-hourly
				      dew-point rtma-dew-point ekdmos-dew-oint
				      head-index wind-chill apparent ekdmo-apparent
				      8-14-day-anomolies monthly-anomolies seasonal-anomolies))

(deftype temperature-unit () `(member fahrenheit
				      celsius))

(defconstant +default-temperature-unit+ 'fahrenheit)

(defstruct temperature-value
  (value nil :type (or null integer))
  (upper-range nil :type (or null (integer -459)))
  (lower-range nil :type (or null (integer -459)))
  (data-source nil :type (or null data-source-type)))

(defstruct temperature-value-list)

(defstruct uncertainty)

(defstruct number-with-equality)

(defstruct temperature-value-with-uncertainty
  (value nil :type (or null temperature-value))
  (uncertainty nil :type (or null uncertainty number-with-equality))
  (data-source nil :type (or null data-source-type)))

(defstruct temperature
  (name nil :type (or null string))
  (type nil :type temperature-type)
  (unit +default-temperature-unit+ :type temperature-unit)
  (time-layout nil :type (or null string))
  (values nil :type (or null array (or temperature-value temperature-value-list temperature-value-with-uncertainty))))

(defun string-to-temperature-type (string)
  (intern (substitute #\- #\SPACE (string-upcase string)) :digital-weather-markup-language))

(defun string-to-temperature-unit (string)
  (if (or (not string)
	  (eq (length string) 0))
      +default-temperature-unit+
      (intern (string-upcase string) :digital-weather-markup-language)))
