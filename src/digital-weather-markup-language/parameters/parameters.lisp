(in-package digital-weather-markup-language)

(deftype categories-type () '(member ekdmos-cdf-percentiles ekdmo-qpf-probabilities))

(deftype data-source-type () '(member ndfd rtma))

(defstruct categories
  (name nil :type (or null string))
  (key nil :type string)
  (values)
  (type nil :type categories-type)
  (probability-type))

(defstruct percipitation)

(defstruct probability-of-percipitation)

(defstruct fire-weather)

(defstruct convective-hazard)

(defstruct climate-anomaly)

(defstruct wind-speed)

(defstruct direction)

(defstruct cloud-amount)

(defstruct humidity)

(defstruct weather)

(defstruct conditions-icon)

(defstruct hazards)

(defstruct worded-forecast)

(defstruct pressure)

(defstruct probailistic-condition)

(defstruct water-state)

(defstruct parameters
  (categories (make-array 0 :element-type 'categories) :type (array categories))
  (temperatures (make-array 0 :element-type 'temperature) :type (array temperature)))

