(in-package digital-weather-markup-language)

(deftype summarization-type () `(member none
					mean maximum minimum
					12hourly 24hourly
					national conus alaska))

(defun string-to-summarization-type (string)
  (cond ((eq nil string) nil)
	((string= string "none") 'none)
	(t (intern (string-upcase string) :digital-weather-markup-language))))
