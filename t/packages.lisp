(defpackage cl-weather-us-tests
  (:use #:cl #:fiveam)
  (:export #:run!
	   #:all-tests

	   *sample-files-directory*))
