(in-package cl-weather-us-tests)

(def-suite all-tests)

(defparameter *sample-files* '(#P"latest_DWMLByDay12hr.txt"
			       #P"latest_DWMLByDay24hr.txt"
			       #P"latest_DWML_glance.txt"
			       #P"latest_DWML.txt"))

(defparameter *sample-files-directory* #P"./data/dwml-samples/")

(dwml-parser:initialize-parser)

(in-suite all-tests)

(defun parse-dwml (file)
  (with-open-file (in (merge-pathnames file *sample-files-directory*))
    (dwml-parser:parse-dwml in)))

(test load-sample-files
  (dolist (file *sample-files*)
    (is (not (eq nil (parse-dwml file))))))

(defun all-tests ()
  (run! 'all-tests))
