(in-package cl-weather-us-tests)

(def-suite digital-weather-markup-language
    :in all-tests)

(in-suite digital-weather-markup-language)

(test string-to-temperature-unit
  (dolist (test '((nil dwml:fahrenheit)
		 ("" dwml:fahrenheit)
		 ("Fahrenheit" dwml:fahrenheit)
		 ("Celsius" dwml:celsius)))
    (is (eq (dwml:string-to-temperature-unit (first test)) (second test)))))
