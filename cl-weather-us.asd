(asdf:defsystem "cl-weather-us"
  :description "A system for getting weather information from the United States’ National Weather Service."
  :version "0.2.1"
  :author "Benjamin Neumann <benjamin+cl-weather-us@neumann.com.hr>"
  :depends-on ("quri"
	       "local-time"
	       "parse-float"
	       "s-xml")
  :serial t
  :components
  ((:module "src"
	    :serial t
	    :components ((:file "packages")
			 (:file "national-digital-forecast-database")
			 (:module "digital-weather-markup-language"
				  :serial t
				  :components ((:file "summarization-type")
					       (:module "parameters"
							:serial t
							:components ((:file "temperature")
								     (:file "parameters")))
					       (:file "location")
					       (:file "time-layout")
					       (:file "digital-weather-markup-language")))
			 (:module "digital-weather-markup-language-parser"
				  :serial t
				  :components ((:file "seed")
					       (:file "shared")
					       (:module "parameters"
							:serial t
							:components ((:file "temperature")
								     (:file "parameters")))
					       (:file "data")
					       (:file "location")
					       (:file "time-layout")
					       (:file "parser")))))))

(asdf:defsystem "cl-weather-us/tests"
  :depends-on ("cl-weather-us"
	       "fiveam")
  :components ((:module "t"
			:serial t
			:components ((:file "packages")
				     (:file "main")
				     (:file "digital-weather-markup-language"))))
  :perform (asdf:test-op (o s)
		    (uiop:symbol-call :cl-weather-us-tests
				      'all-tests)))
