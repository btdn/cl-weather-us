# cl-weather-us

## Introduction

cl-weather-us is an ANSI Common Lisp System for interacting with subsets of the [National Digital Forecast Database (NDFD) REST Web Service](https://graphical.weather.gov/xml/rest.php).

Three packages are provided

1. national-digital-forecast-database generates URL for the web serivce;
2. digital-weather-markup-language represents the responses in Lisp data structures; and
3. digital-weather-markup-language-parser translate a response from the web service into those structures.

## Example Usage on REPL

### National-Digital-Forecast-Database

	;; defaults to the weather at the NOAA headquarters
	CL-USER> (ndfd:to-uri (ndfd:make-unsummarized-query))
	#<QURI.URI.HTTP:URI-HTTPS https://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php?product=glance&begin=2020-05-27T04%3A10%3A05&end=2020-05-27T04%3A10%3A05&Unit=e&lat=38.99225&lon=-77.03064>
	CL-USER> (dex:get (ndfd:to-uri (ndfd:make-unsummarized-query)))
	"<?xml version=\"1.0\"?>
	<dwml version=\"1.0\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"https://graphical.weather.gov/xml/DWMLgen/schema/DWML.xsd\">"
	;; truncated

### Digital-Weather-Markup-Languge-Parser

	CL-USER> (dwml-parser:initialize-parser)
	NIL
	CL-USER> (with-open-file (in #P"./data/dwml-samples/latest_DWML_glance.txt")
	   (dwml-parser:parse-dwml in))
	#S(DIGITAL-WEATHER-MARKUP-LANGUAGE:DATA
	   :PARAMETERS #S(DIGITAL-WEATHER-MARKUP-LANGUAGE::PARAMETERS
                  :CATEGORIES NIL
                  :TEMPERATURES NIL)
	   :MORE-WEATHER-INFORMATION NIL
	   :LOCATIONS #(#S(DIGITAL-WEATHER-MARKUP-LANGUAGE:LOCATION
                   :KEY "point1"
